<?php

// Server version is PHP 5.3.3,
// - no array dereferencing of functions (function()[0] -> use temp var)
// - no array declarations with square brackets ([1,2] -> array(1, 2))

function startsWith($haystack, $needle) {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
}

// get path from commandline or from cgi server info
if (array_key_exists('REQUEST_URI', $_SERVER)) {
  $debug = false;

  // may need more than 30 seconds and more than 128M
  set_time_limit(60);
  ini_set('memory_limit', '256M');

  // if we need to debug
  $query_string = $_SERVER['QUERY_STRING'];
  parse_str($query_string, $entries);
  if (array_key_exists('debug', $entries)) {
    header("content-type: text/plain");
    print "request_uri:   $request_uri\n";
    print "script_name:   $script_name\n";
    print "path:          $path\n";
    print "prefix:        $prefix\n";
    print "script_to_dir: $script_to_dir\n";
    var_dump($_SERVER);
    exit();
  }

  $start = $_REQUEST['start'];
  $end = $_REQUEST['end'];
} else {
  $debug = false;
  $argc = $_SERVER['argc'];
  $argv = $_SERVER['argv'];
  if ($argc <= 2) {
    print "Usage:\n";
    print "  command: export-to-csv.php <start> <end>\n";
    print "  server:  export-to-csv.php?start=<start>&end=<end>\n";
    print "  where <start> and <end> in format YYYY-MM-DD\n";
    exit(1);
  }
  $start = $argv[1];
  $end = $argv[2];
}


header('Content-Type: text/csv');
header('Content-Disposition: attachment;filename=adepo.csv');
#header('Content-Type: text/plain');
while (@ob_end_flush()); // Flush and disable output buffers

date_default_timezone_set('Europe/Zurich');

$files = array_filter(array_merge(glob("./20*/Observations_MOUNT_System_*.txt"),glob("./Observations_MOUNT_System_*.txt")),'is_file');

list($startYear, $startMonth, $startDay) = preg_split ('/-/', $start, 3);
list($endYear, $endMonth, $endDay) = preg_split ('/-/', $end, 3);

if ($debug) {
  echo $startYear.", ".$startMonth.", ".$startDay."\n";
  echo $endYear.", ".$endMonth.", ".$endDay."\n";
}

// FIXME does NOT find the closest startfile...
$startFiles  = preg_grep ('/^\.\/(20..\/)?Observations_MOUNT_System_'.$startYear.'\.'.$startMonth.'\.'.$startDay.'\..+/', $files);
$endFiles  =   preg_grep ('/^\.\/(20..\/)?Observations_MOUNT_System_'.$endYear.'\.'.$endMonth.'\.'.$endDay.'\..+/', $files);

// php 5.1 cannot dereference array of result directly
$keys = array_keys($startFiles);
$first = count($startFiles) == 0 ? 0 : $keys[0];
$last = count($endFiles) == 0 ? count($files) : end(array_keys($endFiles));
$len = $last-$first+1;

if ($debug) {
  echo count($files).", "."\n";
  echo count($startFiles).", "."\n";
}

$files = array_slice($files, $first, $len);

if ($debug) {
  echo count($files).", ".$first.", ".$last.", ".$len."\n";
  exit(1);
}

$sequenceList = array();

// Get and print the header, this may or may not contain all measurements, depending on the included files
foreach($files as $file) {

    // print $file."\n";

    $sequence = null;
    $handle = fopen($file, "r");
    while (($line = fgets($handle)) !== false) {
        if (startsWith($line, "CAM")) {
            $parts = preg_split("/[\s,]+/", trim($line));

            $subParts = preg_split("/_/", trim($parts[0]));
            $sequence = $subParts[1];

            if (!in_array($sequence, $sequenceList)) {
              $sequenceList[] = $sequence;
            }
        }
    }
    fclose($handle);
}

// print_r($sequenceList);

$header = "datetime";
foreach ($sequenceList as $sequence) {
  $header .= ', X_'.$sequence;
  $header .= ', Y_'.$sequence;
  $header .= ', Z_'.$sequence;
  $header .= ', stdX_'.$sequence;
  $header .= ', stdY_'.$sequence;
  $header .= ', stdZ_'.$sequence;
}
print($header."\n");

// Now read all files again and output a line per file
foreach($files as $file) {

    $sequence = null;
    $table = array();

    $handle = fopen($file, "r");
    while (($line = fgets($handle)) !== false) {

    	  if (startsWith($line, "CAM")) {
	          $parts = preg_split("/[\s,]+/", trim($line));

            $d = strptime($parts[1], '%Y.%m.%d.%H.%M.%S');
            $timestamp = mktime($d['tm_hour'], $d['tm_min'], $d['tm_sec'], $d['tm_mon']+1, $d['tm_mday'], 1900 + $d['tm_year']);
            $datetime = date("m/d/Y H:i:s", $timestamp);

// only in PHP > 5.2
//            $date = DateTime::createFromFormat('Y.m.d.H.i.s', $parts[1]);
//            $records = $date->format('m/d/Y H:i:s');

            $subParts = preg_split("/_/", trim($parts[0]));
            $sequence = $subParts[1];

            $record = ', '.$parts[2];
            $record .= ', '.$parts[3];
            $record .= ', '.$parts[4];
            $record .= ', '.$parts[5];
            $record .= ', '.$parts[6];
            $record .= ', '.$parts[7];

            $table[$datetime][$sequence] = $record;
        }
    }
    fclose($handle);

    $datetimes = array_keys($table);
    foreach ($datetimes as $datetime) {
      print($datetime);

      foreach ($sequenceList as $sequence) {
        $record = $table[$datetime][$sequence];
        if ($record == null) {
          // does not exist
          print(", 0, 0, 0, 0, 0, 0");
        } else {
          print($record);
        }
      }

      print("\n");
      flush();
    }
}

?>
